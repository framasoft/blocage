# Page de blocage de Framasoft

Page vers laquelle nous redirigeons les utilisateurs bloqués sur nos services, que ce soit pour raison technique ou violation de nos [CGU](https://framasoft.org/fr/cgu/).

# Licence

© 2019 Framasoft AGPLv3

Voir le fichier [LICENSE](LICENSE)
